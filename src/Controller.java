import java.io.File;

import weka.core.Instances;
import weka.core.converters.CSVLoader;

public class Controller {

	private View view;
	private GameModel model;
	private Instances insts;
	private Bot bot;
	private String message;

	public Controller(View v){
		this.view = v;
		this.model = new GameModel();
		this.insts = loadInstances("./tic-tac-toe.csv", ";");
		this.insts.setClassIndex(insts.numAttributes() - 1);
		this.bot = new J48Bot(this.insts, "x", "x", "o", "true");
		this.bot.init();
	}
	
	public void setUserFirst(Boolean state) {
		GameModel.PlayerTypes humanType = GameModel.PlayerTypes.HUMAN;
		GameModel.PlayerTypes botType = GameModel.PlayerTypes.BOT;
		if (state) {
			model.setPlayers(new GameModel.PlayerTypes[] {humanType, botType});
			bot.setSettings("o", "x", "false");
		} else {
			model.setPlayers(new GameModel.PlayerTypes[] {botType, humanType});
			bot.setSettings("x", "o", "true");
		}
	}

	private Instances loadInstances(String source, String separator) {
		CSVLoader loader = new CSVLoader();
		loader.setFieldSeparator(separator);

		Instances instances = null;
		try {
			loader.setSource(new File(source));
			instances = loader.getDataSet();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}

		return instances;
	}

	public void continueGame() {
		if(model.hasWinner()){
			model.nextTurn();
			String mark = model.getPlayerMark();
			message = "Player " + mark + " wins!";
			System.out.println("Player " + mark + " wins!");
			view.showStart(message);
			return;
		}
		else if(model.boardIsFull()){
			message = "Tie!";
			System.out.println("Tie!");
			view.showStart(message);
			return;
		}
		if (model.playerIsBot()) {
			view.setInteractive(false);
			int index = bot.getMove(model.getBoard());
			if(!updateGame(index)){
				System.out.println("Bot tries to cheat!");
			}
			continueGame();
		} else {
			view.setInteractive(true);
		}
	}

	public boolean updateGame(int index){
		if(model.setValue(index)){
			view.setMark(index, model.getPlayerMark());
			model.nextTurn();
			return true;
		}
		return false;
	}

	public void newGame(){
		System.out.println("New game!");
		model.reset();
		view.reset();
	}

	public void printBoard(String[] board) {
		String output = "";
		int nro = 1;
		for(String mark : board){
			output += mark;
			if(nro%3==0){
				output += "\n";
			} else {
				output += "\t";
			}
			nro++;
		}
		System.out.println(output);
	}

	public void winningCheckStateChanged(Boolean state) {
		bot.setWinningCheck(state);
	}

}
