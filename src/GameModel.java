import java.util.ArrayList;
import java.util.Arrays;

public class GameModel {
	private String[] board;
	private String[] marks;
	private PlayerTypes[] players;
	int currentIndex;
	ArrayList<Integer[]> patterns;
	public static enum PlayerTypes {
		BOT, HUMAN
	}
	
	public GameModel(PlayerTypes[] players) {
		this();
		this.players = players;
	}

	public GameModel(){
		this.players = new PlayerTypes[] {PlayerTypes.BOT, PlayerTypes.HUMAN};
		board = new String[9];
		Arrays.fill(board, "b");
		marks = new String[] {"x","o"};
		currentIndex = 0;
		patterns = new ArrayList<Integer[]>() {{
			// Horizontal
			add(new Integer[] {0, 1, 2});
			add(new Integer[] {3, 4, 5});
			add(new Integer[] {6, 7, 8});

			// Vertical
			add(new Integer[] {0, 3, 6});
			add(new Integer[] {1, 4, 7});
			add(new Integer[] {2, 5, 8});

			// Diagonal
			add(new Integer[] {0, 4, 8});
			add(new Integer[] {2, 4, 6});
		}};
	}
	
	public void setPlayers(PlayerTypes[] players) {
		this.players = players;
	}

	public boolean setValue(int index){
		if(board[index].equals("b")){
			board[index] = marks[currentIndex];
			return true;
		} else {
			return false;
		}
	}

	public String getPlayerMark() {
		return marks[currentIndex];
	}

	public void nextTurn() {
		currentIndex = (currentIndex + 1) % marks.length;
	}

	public boolean playerIsBot() {
		return this.players[currentIndex] == PlayerTypes.BOT;
	}

	public String[] getBoard() {
		return this.board;
	}

	public boolean hasWinner(){
		for(Integer[] pattern : patterns){
			if(board[pattern[0]].equals("b")){
				continue;
			}
			if(board[pattern[0]].equals(board[pattern[1]]) &&
					board[pattern[0]].equals(board[pattern[2]])){
				return true;
			}
		}
		return false;
	}

	public boolean boardIsFull() {
		for(String mark : board){
			if(mark.equals("b")) {
				return false;
			}
		}
		return true;
	}

	public void reset(){
		Arrays.fill(board, "b");
		currentIndex = 0;
	}


}
