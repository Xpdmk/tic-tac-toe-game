import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

import weka.classifiers.evaluation.Evaluation;
import weka.classifiers.trees.J48;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SparseInstance;
import weka.core.converters.CSVLoader;

public class Classifier {
	
	public static String B = "b";
	public static String X = "x";
	public static String O = "o";
	
	public static Instances getCSVInstances(String source, String separator) throws Exception {
		CSVLoader loader = new CSVLoader();
		loader.setFieldSeparator(separator);
		
		Instances instances = null;
		loader.setSource(new File(source));
		instances = loader.getDataSet();
		
		return instances;
	}
	
	public static ArrayList<Integer> instanceToIndexArrayList(Instance instance) {
		ArrayList<Integer> indexes = new ArrayList<>();
		ArrayList<Attribute> attrs = getAttributes(instance);
		for (int i = 0; i < instance.numValues(); i++) {
			indexes.add(((Double) instance.value(i)).intValue());
		}
		return indexes;
	}
	
	public static ArrayList<String> instanceToMarkArrayList(Instance instance) {
		ArrayList<String> marks = new ArrayList<>();
		ArrayList<Attribute> attrs = getAttributes(instance);
		for (int i = 0; i < instance.numValues(); i++) {
			int valueIndex = ((Double) instance.value(i)).intValue();
			String value = attrs.get(i).value(valueIndex);
			marks.add(value);
		}
		return marks;
	}
	
	public static ArrayList<Attribute> getAttributes(Instance inst) {
		Attribute[] attrs = new Attribute[inst.numAttributes()];
		for (int i = 0; i < attrs.length; i++) {
			attrs[i] = inst.attribute(i);
		}
		return new ArrayList<>(Arrays.asList(attrs));
	}
	
	public static Instance createInstance(String[] gameValues, ArrayList<Attribute> attrs) {
		Instances dummyInstances = new Instances("Dummy", attrs, 0);
		double[] attrValueIndexes = new double[attrs.size()];
		for (int i = 0; i < attrs.size(); i++) {
			Attribute attr = attrs.get(i);
			int index = attr.indexOfValue(gameValues[i]);
			attrValueIndexes[i] = index;
		}
		Instance newInst = new DenseInstance(0.0, attrValueIndexes);
		dummyInstances.add(newInst);
		dummyInstances.setClassIndex(attrs.size() - 1);
		return dummyInstances.firstInstance();
	}
	
	public static ArrayList<Integer> getEmptySpots(Instance inst) {
		ArrayList<Integer> emptyIndexes = new ArrayList<>();
		ArrayList<Attribute> attrs = getAttributes(inst);
		
		double BIndex = 0;
		double valueIndex = 0;
		Attribute attr = null;
		for (int i = 0; i < attrs.size(); i++) {
			attr = attrs.get(i);
			BIndex = attr.indexOfValue(B);
			valueIndex = inst.value(attr);
			if (BIndex == valueIndex) {
				emptyIndexes.add(i);
			}
		}
		return emptyIndexes;
	}
	
	/**
	 * Generates instances with playerMark string on attributes that have 'b' (B) as the value
	*/
	public static Instances generateNextInstances(Instance instance, 
ArrayList<Attribute> attributes, String playerMark, int classIndex, String classValue) {
		ArrayList<String> values = instanceToMarkArrayList(instance);
		Instances nextInstances = new Instances("Next instances", attributes, 0);
		double[] dummyArray = new double[attributes.size()];
		for (int i = 0; i < values.size(); i++) {
			dummyArray[i] = attributes.get(i).indexOfValue(values.get(i));
		}
		dummyArray[classIndex] = attributes.get(classIndex).indexOfValue(classValue);
		
		int playerMarkIndex = 0; // Initialize variable
		System.out.println(values.toString());
		for (int i = 0; i < attributes.size(); i++) {
			
			if (values.get(i).equals(B) && i != classIndex) {
				// Get index of player mark in attribute values
				playerMarkIndex = attributes.get(i).indexOfValue(playerMark);
				
				// Create new instance of dummy array
				System.arraycopy(dummyArray, 0, dummyArray, 0, dummyArray.length);
				
				// Create new instance with playerMark on attribute i
				Instance nextInstance = new DenseInstance(0.0, dummyArray);
				nextInstance.setValue(i, playerMarkIndex);
				nextInstances.add(nextInstance);
			}
		}
		
		nextInstances.setClassIndex(classIndex);
		return nextInstances;
	}

	public static void main(String[] args) throws Exception {
		Instances instances = getCSVInstances("./tic-tac-toe.csv", ";");
		ArrayList<Attribute> attrs = getAttributes(instances.instance(0));
		
		int targetAttrIndex = instances.numAttributes() - 1;
		Attribute targetAttr = instances.attribute(targetAttrIndex);
		String targetAttrValue = "true";
		
		String playerMark = X;
		String[] currentGameValues = new String[]
		{ 
				X, B, O,
				B, B, X,
				B, B, X,
				targetAttrValue
	  };
		
		Instance current = createInstance(currentGameValues, attrs);
		
		System.out.println(current.toString());
		
		System.out.println("Current:");
		printGrid(current);
		
		// Generate possible next instances
		Instances nextInstances = generateNextInstances(current, attrs, playerMark, targetAttrIndex, targetAttrValue);
		
		// Create tree
		String[] options = new String[] {"-M", "2"};
		J48 tree = new J48();
		tree.setOptions(options);
		instances.setClassIndex(instances.numAttributes() - 1);
		tree.buildClassifier(instances);
		
		
		int targetAttrValueCount = targetAttr.numValues();
		int nextInstanceCount = nextInstances.size();
		double[][] confidences = new double[nextInstanceCount][targetAttrValueCount];
		for (int instIndex = 0; instIndex < nextInstances.size(); instIndex++) {
			Instance inst = nextInstances.get(instIndex);
			inst.setClassValue(targetAttrIndex);
			double[] dists = tree.distributionForInstance(inst);
			for (int distIndex = 0; distIndex < dists.length; distIndex++) {
				confidences[instIndex][distIndex] = dists[distIndex];
			}
			System.out.println(confidences[instIndex][0] + ", " + confidences[instIndex][1]);
		}
		
		System.out.println("End");
	}
	
	public static void printGrid(Instance inst) {
		ArrayList<Attribute> attrs = getAttributes(inst);
		for (int i = 0; i < 9; i++) {
			int index = (int) inst.value(i);
			String value = attrs.get(i).value(index);
			System.out.print(value);
			if ((i+1) % 3 == 0) {
				System.out.println();
			}
		}
	}
	
	public static int checkWinningMove(Instance instance, String playerMark) {
		ArrayList<Integer[]> patterns = new ArrayList<Integer[]>() {{
			// Horizontal
			add(new Integer[] {0, 1, 2});
			add(new Integer[] {3, 4, 5});
			add(new Integer[] {6, 7, 8});
			
			// Vertical
			add(new Integer[] {0, 3, 6});
			add(new Integer[] {1, 4, 7});
			add(new Integer[] {2, 5, 8});
			
			// Diagonal
			add(new Integer[] {0, 4, 8});
			add(new Integer[] {2, 4, 6});
		}};
		
		ArrayList<String> instanceMarks = Classifier.instanceToMarkArrayList(instance);
		instanceMarks.remove(instanceMarks.size() - 1);
		
		// Collect marks from pattern indexes
		String[][] patternMarks = new String[8][3];
		for (int i = 0; i < patterns.size(); i++) {
			Integer[] pattern = patterns.get(i);
			String[] marks = new String[pattern.length];
			for (int k = 0; k < marks.length; k++) {
				marks[k] = instanceMarks.get(pattern[k]);
			}
			patternMarks[i] = marks;
		}
		
		// Check patterns of marks to have at most 1 blank and player marks otherwise
		PATTERN_MARKS_LOOP:
		for (int i = 0; i < patternMarks.length; i++) {
			int blankIndex = -1;
			String[] marks = patternMarks[i];
			for (int k = 0; k < marks.length; k++) {
				String mark = marks[k];
				if (mark.equals(B)) {
					if (blankIndex >= 0) {
						blankIndex = -1;
						continue PATTERN_MARKS_LOOP;
					} else {
						blankIndex = k;
					}
				} else if (mark.equals(playerMark)) {
					continue;
				} else {
					blankIndex = -1;
					continue PATTERN_MARKS_LOOP;
				}
			}
			if (blankIndex >= 0) {
				return patterns.get(i)[blankIndex];
			}
		}
		
		return -1;
	}

}
